<?php


namespace BeeJee\TodoList\Controllers;


use BeeJee\TodoList\Core\App;
use BeeJee\TodoList\Core\HTMLResponse;
use BeeJee\TodoList\Core\RedirectResponse;
use BeeJee\TodoList\Core\Request;
use BeeJee\TodoList\Core\View;

class LoginController extends BaseController
{
    private const USER = 'admin';
    private const PASS = '123';

    protected const WRONG_USER_OR_PASS = 'User or password is wrong.';

    public function login()
    {
        $content = [ 'main' => $this->getLoginForm() ];
        return new HTMLResponse($this->render($content));
    }

    public function loginFormSubmit()
    {
        if ($this->validateLoginForm()) {
            App::auth()->logIn();
            return new RedirectResponse();
        } else {
            return $this->login();
        }
    }

    public function logout()
    {
        App::auth()->logOut();
        return new RedirectResponse();
    }

    protected function validateLoginForm()
    {
        return $this->validateFormFields(['name', 'pass']) && $this->validateUser();
    }

    protected function validateUser()
    {
        $correctUser = $this->requestParams['name'] === static::USER &&
            $this->requestParams['pass'] === static::PASS;
        if (!$correctUser) {
            $this->validationErrors[] = static::WRONG_USER_OR_PASS;
        }
        return $correctUser;
    }

    protected function getLoginForm()
    {
        $token = $this->generateFormToken();
        $args['formValues'] = [
            'name'  => $this->requestParams['name'] ?? '',
            'token' => $token,
        ];
        $args['validationErrors'] = $this->validationErrors;
        return new View('login-form', $args);
    }
}