<?php


namespace BeeJee\TodoList\Controllers;


use BeeJee\TodoList\Core\App;
use BeeJee\TodoList\Core\HTMLResponse;
use BeeJee\TodoList\Core\Request;
use BeeJee\TodoList\Core\View;
use BeeJee\TodoList\Entity\Task;

class HomePageController extends BaseController
{
    protected const FORM_FIELD_INVALID = 'Form field {field} value is invalid.';
    protected const EMAIL_IS_INVALID = 'Email field is invalid';
    protected const TASK_CREATED = 'Task has been created';

    protected $allowedOrder = ['name', 'email', 'status'];

    protected $tasksLimit = 3;

    protected $page = 0;

    protected $currentOrder;

    protected $requestParams;

    public function index()
    {
        $tasKQuery = Task::select();

        // Add order.
        if (isset($this->requestQuery['order'])) {
            $direction = strtoupper($this->requestQuery['direction']);
            $direction = ($direction === 'DESC') ? $direction : 'ASC';
            if (in_array($this->requestQuery['order'], $this->allowedOrder)) {
                $tasKQuery->orderBy($this->requestQuery['order'], $direction);
            }
            if ($this->requestQuery['order'] === 'status') {
                $tasKQuery->orderBy(Task::PRIMARY_KEY, $direction);
            }
        } else {
            $tasKQuery->orderBy(Task::PRIMARY_KEY, 'DESC');
        }

        // Add pagination.
        if (isset($this->requestQuery['page'])) {
            $page = $this->sanitizePage($this->requestQuery['page']);
            if (!empty($page)) {
                $this->page = $page;
                $tasKQuery->limit($this->tasksLimit, $this->tasksLimit * $this->page);
            } else {
                $tasKQuery->limit($this->tasksLimit);
            }
        } else {
            $tasKQuery->limit($this->tasksLimit);
        }

        // Load Tasks.
        $tasks = Task::fetchAll();
        $content['main'] = $this->getTaskForm();
        if (!empty($tasks)) {
            $taskViewArgs = [
                'tasks' => $tasks,
                'filter' => $this->getFilters(),
                'pager' => $this->getPager(),
                'isAdmin' => App::auth()->isLoggedIn(),
            ];
            $content['content'] = new View('tasks', $taskViewArgs);
        }

        $content['messages'] = $this->messages;
        $renderedContent = $this->render($content);
        return new HTMLResponse($renderedContent);
    }

    public function formSubmit()
    {
        if ($this->validateTaskForm($this->requestParams)) {
            $task = new Task();
            $task['name'] = $this->requestParams['name'];
            $task['email'] = $this->requestParams['email'];
            $task['text'] = $this->requestParams['text'];
            $this->messages[] = self::TASK_CREATED;
            $task->save();
            $this->requestParams = [];
        }
        return $this->index();
    }

    protected function sanitizePage($page)
    {
       return (is_numeric($page) && ($page > 0)) ? (int) $page : 0;
    }

    protected function getTaskForm()
    {
        $token = $this->generateFormToken();
        $args['formValues'] = [
            'name'  => $this->requestParams['name'] ?? '',
            'email' => $this->requestParams['email'] ?? '',
            'text'  => $this->requestParams['text'] ?? '',
            'token' => $token,
        ];
        $args['validationErrors'] = $this->validationErrors;
        return new View('task-form', $args);
    }

    protected function getPager()
    {
        $result = '';
        $total = Task::total();
        if ($total > $this->tasksLimit) {
            $pages = (int) ($total / $this->tasksLimit);
            $rest = $total % $this->tasksLimit;
            if ($rest > 0) {
                $pages++;
            }

            $args = [
                'pages' => $pages,
                'total' => $total,
                'page' => $this->page,
                'isFirst' => ($this->page === 0),
                'isLast' => ($this->page === ($pages - 1))
            ];

            if ($args['isFirst']) {
                $args['prevLink'] = '#';
            } else {
                $linkQuery = $this->requestQuery;
                $linkQuery['page'] = $this->page - 1;
                $args['prevLink'] = '/?' . http_build_query($linkQuery);
            }

            if ($args['isLast']) {
                $args['nextLink'] = '#';
            } else {
                $linkQuery = $this->requestQuery;
                $linkQuery['page'] = $this->page + 1;
                $args['nextLink'] = '/?' . http_build_query($linkQuery);
            }

            for ($i = 0; $i < $pages; $i++) {
                $linkQuery = $this->requestQuery;
                $linkQuery['page'] = $i;
                $args['pageLinks'][$i] = '/?' .http_build_query($linkQuery);
            }

            $result = new View('pager', $args);
        }

        return $result;
    }

    protected function validateTaskForm($values)
    {
        $fields = [ 'name', 'email', 'text'];
        return $this->validateFormFields($fields) && $this->validateEmail();
    }

    protected function validateEmail()
    {
        $isValid = filter_var($this->requestParams['email'], FILTER_VALIDATE_EMAIL);
        if (!$isValid) {
            $this->validationErrors[] = self::EMAIL_IS_INVALID;
        }
        return $isValid;
    }

    protected function getFilters(): array
    {
        if (isset($this->requestQuery['order'])) {
            $this->currentOrder = $this->requestQuery['order'];
        }
        $currentOrder = $this->requestQuery['order'] ?? null;
        $direction = $this->requestQuery['direction'] ?? null;
        $filter = [];
        foreach ($this->allowedOrder as $name) {
            $filterQuery = [
                'order' => $name,
                'direction' => 'ASC',
            ];
            $filter[$name]['class'] = '';
            $filter[$name]['icon'] = '';
            if (isset($currentOrder) && $currentOrder === $name) {
                if (isset($direction)) {
                    $filterQuery['direction'] = ($direction === 'ASC') ? 'DESC' : 'ASC';
                    if ($direction === 'ASC') {
                        $filter[$name]['icon'] = '<i class="fa fa-sort-up"></i>';
                    } else {
                        $filter[$name]['icon'] = '<i class="fa fa-sort-down"></i>';
                    }
                }
                $filter[$name]['class'] = 'table-primary';
            }
            if (isset($this->page)) {
                $filterQuery['page'] = $this->page;
            }
            $filter[$name]['link'] = '/?' . http_build_query($filterQuery);
        }
        return $filter;
    }
}