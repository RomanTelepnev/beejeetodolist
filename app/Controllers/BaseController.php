<?php


namespace BeeJee\TodoList\Controllers;


use BeeJee\TodoList\Core\App;
use BeeJee\TodoList\Core\View;

class BaseController
{
    protected const FORM_TOKEN_INVALID = 'Form is invalid.';
    protected const FORM_FIELD_EMPTY = 'Form field {field} is empty.';

    protected $baseView = 'html';

    protected $request;

    protected $requestQuery;

    protected $validationErrors = [];

    protected $messages = [];

    protected $requestParams;

    public function create()
    {
        $this->requestQuery = $this->request->getQuery();
        $this->requestParams = $this->request->getAll();
    }

    protected function render($content)
    {
        if (App::auth()->isLoggedIn()) {
            $content['authLink'] = '/logout';
            $content['authLinkName'] = 'Logout';
        } else {
            $content['authLink'] = '/login';
            $content['authLinkName'] = 'Login';
        }
        $baseView = new View($this->baseView, $content);
        return $baseView->getContent();
    }

    protected function generateFormToken()
    {
        $token = md5(time() . '42');
        $_SESSION['token'] = $token;
        return $token;
    }

    protected function validFormToken()
    {
        return !empty($this->requestParams['token']) && ($this->requestParams['token'] === $_SESSION['token']);
    }

    protected function validateFormFields($fields)
    {
        $fieldValidation = [];
        if ($this->validFormToken()) {
            foreach ($fields as $field) {
                if (!isset($this->requestParams[$field]) || $this->requestParams[$field] === '') {
                    $fieldValidation[] = str_replace('{field}', $field, self::FORM_FIELD_EMPTY);
                }
            }
        } else {
            $fieldValidation[] = static::FORM_TOKEN_INVALID;
        }
        $this->validationErrors = array_merge($this->validationErrors, $fieldValidation);
        return empty($fieldValidation);
    }
}