<?php


namespace BeeJee\TodoList\Controllers;


use BeeJee\TodoList\Core\App;
use BeeJee\TodoList\Core\HTMLResponse;
use BeeJee\TodoList\Core\RedirectResponse;
use BeeJee\TodoList\Core\Request;
use BeeJee\TodoList\Core\View;
use BeeJee\TodoList\Entity\Task;

class TaskController extends BaseController
{
    protected const TASK_UPDATED = 'The task has been updated.';

    public function editForm($taskId)
    {
        if (App::auth()->isLoggedIn()) {
            $content = ['main' => $this->getTaskEditForm($taskId) ];
            return new HTMLResponse($this->render($content));
        } else {
            return new RedirectResponse('/login');
        }
    }

    public function editFormSubmit($taskId)
    {
        if (App::auth()->isLoggedIn()) {
            $fields = ['text'];
            if ($this->validateFormFields($fields)) {
                $task = Task::find($taskId);
                if ($this->requestParams['text'] !== $task['text']) {
                    $task['edited'] = 1;
                }
                $task['text'] = $this->requestParams['text'];
                if (!empty($this->requestParams['status'])) {
                    if ($this->requestParams['status'] == 1) {
                        $task['status'] = $this->requestParams['status'];
                    }
                } else {
                    $task['status'] = 0;
                }

                $task->save();
                $this->messages[] = self::TASK_UPDATED;
                $this->requestParams = [];
                return $this->editForm($taskId);
            } else {
                return $this->editForm($taskId);
            }
        } else {
            return new RedirectResponse('/login');;
        }
    }

    protected function getTaskEditForm($taskId)
    {
        $task = Task::find($taskId);
        $formValues = $this->requestParams;
        $defaults = [
          'name' => $task['name'],
          'email' => $task['email'],
          'text' => $task['text'],
          'status' => $task['status'],
        ];
        $formValues += $defaults;
        $formValues['token'] = $this->generateFormToken();
        $viewArgs = [
            'formValues' => $formValues,
            'messages' => $this->messages,
            'path' => Request::path(),
        ];
        return new View('task-edit-form', $viewArgs);
    }
}