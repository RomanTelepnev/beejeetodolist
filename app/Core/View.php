<?php


namespace BeeJee\TodoList\Core;


class View implements IView
{
    const TEMPLATES_DIR = __DIR__ . '/../views';
    const TEMPLATE_EXT = 'tpl.php';

    private $name;

    private $args;

    private $template;

    private $content;

    public function __construct($name, $args = array())
    {
        $this->name = $name;
        $this->args = $args;
    }

    private function loadTemplate()
    {
        ob_start();
        $file = self::TEMPLATES_DIR . '/' . $this->name . '.' . self::TEMPLATE_EXT;
        if (file_exists($file)) {
            extract($this->args);
            include $file;
        }
        $this->content = ob_get_clean();
    }

    public function getContent(): string
    {
        if (!isset($this->content)) {
            $this->loadTemplate();
        }
        return $this->content;
    }

    public function __toString()
    {
        return $this->getContent();
    }
}