<?php


namespace BeeJee\TodoList\Core;


class HTMLResponse implements IResponse
{
    protected $content;

    public function __construct($content = null)
    {
        $this->content = $content;
    }

    public function send()
    {
        echo $this->content;
    }
}