<?php

namespace BeeJee\TodoList\Core;

class Request
{

    public static function path()
    {
        $path = trim(parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH), '/');
        return $path ? $path : '/';
    }

    public static function requestMethod()
    {
        return $_SERVER['REQUEST_METHOD'];
    }

    public static function back(string $where = '')
    {
        if (empty($where)) {
            $where = isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : '/';
        }
        
        header('Location: ' . $where);
    }

    public function getAll(): array
    {
        $request = $GLOBALS['_' . self::requestMethod()];
        self::sanitizeRequest($request);
        return $request;
    }

    public function getQuery(): array
    {
        $request = $_GET;
        self::sanitizeRequest($request);
        return $request;
    }

    protected static function sanitizeRequest(&$request)
    {
        foreach ($request as $item => $value) {
            if (is_string($value)) {
                $value = trim($value);
                $value = htmlspecialchars($value);
                $value = stripcslashes($value);
                $request[$item] = $value;
            } elseif (is_array($value)) {
                self::sanitizeRequest($value);
            }
        }
    }
}