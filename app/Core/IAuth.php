<?php


namespace BeeJee\TodoList\Core;


interface IAuth
{
    public function isLoggedIn(): bool;

    public function logIn();

    public function logOut();
}