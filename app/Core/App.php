<?php


namespace BeeJee\TodoList\Core;


class App
{
    private static $instance;

    private $settings = [];

    private $router;

    private $db;

    private $auth;

    public function __construct()
    {
        $this->loadAppSettings();
        $this->loadRoutes();
        $this->loadDb();
        $this->loadAuth();
        session_start();
        self::$instance = $this;
    }

    public static function getSetting($offset)
    {
        $setting = null;
        if (array_key_exists($offset, self::$instance->settings)) {
            $setting = self::$instance->settings[$offset];
        }
        return $setting;
    }


    protected function loadAppSettings(): void
    {
        $settingsFile = __DIR__ . '/../settings/app.php';
        if (file_exists($settingsFile)) {
            include_once $settingsFile;
            if (!empty($settings)) {
                $this->settings = $settings;
            }
        }
    }

    protected function loadRoutes()
    {
        if ($this->settings['router'] && class_exists($this->settings['router'])) {
            $router = new $this->settings['router']();
            if ($router instanceof IRouter) {
                $routesFile = __DIR__ . '/../settings/routes.php';
                if (file_exists($routesFile)) {
                    require_once $routesFile;
                }
                $this->router = $router;
            }
        }
    }

    public function processRequest(): IResponse
    {
        $response = $this->router->processRequest();
        return $response;
    }

    public static function getDB(): IDB
    {
        return self::$instance->db;
    }

    public static function auth(): IAuth
    {
        return self::$instance->auth;
    }

    protected function loadDb()
    {
        $dbDriver = $this->settings['dbDriver'];
        $this->db = $dbDriver::load($this->settings['db']);
    }

    protected function loadAuth()
    {
        $auth = $this->settings['auth'];
        $this->auth = new $auth;
    }

}