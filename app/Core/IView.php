<?php


namespace BeeJee\TodoList\Core;


interface IView
{
    public function __construct($name, $args);

    public function getContent(): string;
}