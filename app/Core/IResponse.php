<?php


namespace BeeJee\TodoList\Core;


interface IResponse
{
    public function __construct($content = null);

    public function send();
}