<?php


namespace BeeJee\TodoList\Core;


class Auth implements IAuth
{
    public function isLoggedIn(): bool
    {
        return !empty($_SESSION['user']);
    }

    public function logIn()
    {
        $_SESSION['user'] = 1;
    }

    public function logOut()
    {
        unset($_SESSION['user']);
    }
}