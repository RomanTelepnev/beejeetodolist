<?php


namespace BeeJee\TodoList\Core;


interface IRouter
{
    public function get($url, $controllerMethod): IRouter;

    public function post($url, $controllerMethod): IRouter;
}