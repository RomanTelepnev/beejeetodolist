<?php


namespace BeeJee\TodoList\Core;


interface IEntity
{
    public static function find($id): IEntity;

    public function save();
}