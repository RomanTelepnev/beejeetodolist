<?php


namespace BeeJee\TodoList\Core;


class RedirectResponse implements IResponse
{
    protected $path;

    public function __construct($content = null)
    {
        $this->path = $content ?? '/';
    }

    public function send()
    {
        Request::back($this->path);
    }
}