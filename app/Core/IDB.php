<?php


namespace BeeJee\TodoList\Core;


interface IDB
{
    /**
     * @param string $table
     * @param array $fields
     *
     * Builds select query
     *
     * @return $this
     */
    public function select(string $table, array $fields = []): IDB;

    /**
     * @param string $field
     * @param $value
     * @param string $operator
     * @param string $condition
     *
     * Adds 'where' constraints
     *
     * @return $this
     */
    public function where(string $field, $value, string $operator = '=', string $condition = 'AND' ): IDB;

    public function whereIn(string $filed, array $values, string $condition = 'AND'): IDB;

    /**
     * Gets fetched result
     * @return array
     */
    public function get(): array;

    public function first();

    public function getLastId();

    public function getColumns($table): array;


    /**
     * @param string $table
     * @param string $rightField
     * @param string $leftField
     *
     * Adds to query JOIN statement
     *
     * @param string $operator
     * @return $this
     */
    public function join(string $table, string $leftField , string $rightField, string $operator);
    public  function leftJoin(string $table, string $leftField , string $rightField, string $operator);

    public function limit(int $offset, int $limit);


    /**
     * @param string $table
     * @param array $values [field_name => field_value]
     *
     * Inserts data in table
     *
     * @return int
     */
    public function insert(string $table, array $values): int;

    public function delete(string $table);

    public function execute();

    /**
     * Builds Update query
     *
     * @param string $table
     * @param array $fieldsAndValues [ field_name => value ]
     * @return $this
     */
    public function update(string $table, array $fieldsAndValues);

    public function orderBy(string $field, string $ord);
}