<?php

namespace BeeJee\TodoList\Core;

class Router implements IRouter
{
    private $response;

    protected static $routes = [
        'GET' => [],
        'POST' => []
    ];

    public function get($url, $controllerMethod): IRouter
    {
        $this->saveRoute(self::$routes['GET'], $url, $controllerMethod);
        return $this;
    }

    public function post($url, $controllerMethod): IRouter
    {
        $this->saveRoute(self::$routes['POST'], $url, $controllerMethod);
        return $this;
    }

    protected function saveRoute(&$method, $url, $controllerMethod)
    {
        if (preg_match_all('@{(?P<keys>\w+)}@', $url, $params)){
            foreach ($params[0] as $param){
                $url = str_replace($param, '(.+)', $url);
            }
            $url = trim($url, '/');
        }
        $method[$url]['controller'] = $controllerMethod;
    }


    public function processRequest(): IResponse
    {
        $path = Request::path();
        $method = Request::requestMethod();

        if (array_key_exists($path, self::$routes[$method])) {
            $this->executeHandler($method, $path);
        } else {
            foreach (self::$routes[$method] as $pattern => $callback) {
                if (preg_match("@^{$pattern}$@", $path, $args)) {
                    unset($args[0]);
                    $this->executeHandler($method, $pattern, $args);
                }
            }
        }

        if (!isset($this->response)) {
            $errorController = App::getSetting('defaultErrorController');
            $this->response = (new $errorController)->errorPage();
        }

        return $this->response;
    }

    protected function executeHandler($method, $path, $args = [])
    {
        $callback = explode('@', self::$routes[$method][$path]['controller']);
        $callback[] = $args;
        $this->response = $this->callController(...$callback);
        if (!($this->response instanceof IResponse)) {
            $errorController = App::getSetting('defaultErrorController');
            if (is_numeric($this->response)) {
                $this->response = (new $errorController)->errorPage($this->response);
            } else {
                $this->response = (new $errorController)->errorPage();
            }

        }
    }

    protected function callController($class, $method, $args = [])
    {
        $class = 'BeeJee\\TodoList\\Controllers\\' . $class;
        $controller = new $class;
        try {
            $controllerReflection = new \ReflectionClass($class);
            $request = $controllerReflection->getProperty('request');
            $request->setAccessible(true);
            $request->setValue($controller, new Request());
        } catch (\Exception $e) {}

        $controller->create();
        return $controller->$method(...$args);
    }
}