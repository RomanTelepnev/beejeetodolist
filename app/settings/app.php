<?php
/**
 * @file General app settings.
 */

$settings = [
    'db' => [
        'type'    => 'mysql',
        'host'    => 'localhost',
        'dbname'  => 'todo',
        'login'   => 'root',
        'password'=> '123456',
        'options' => [
            PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
            PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION
        ]
    ],
    'dbDriver' => 'BeeJee\\TodoList\\Core\\DB',
    'router' => 'BeeJee\\TodoList\\Core\\Router',
    'defaultErrorController' => 'BeeJee\\TodoList\\Controllers\\ErrorController',
    'auth' => 'BeeJee\\TodoList\\Core\\Auth',
];