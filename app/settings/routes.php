<?php

$router->get('/','HomePageController@index');
$router->post('/','HomePageController@formSubmit');
$router->get('login', 'LoginController@login');
$router->get('logout', 'LoginController@logout');
$router->post('login', 'LoginController@loginFormSubmit');
$router->get('task/{task}', 'TaskController@editForm');
$router->post('task/{task}', 'TaskController@editFormSubmit');