<?php


namespace BeeJee\TodoList\Entity;


use BeeJee\TodoList\Core\App;
use BeeJee\TodoList\Core\IEntity;

class Task extends Entity
{
    public const TABLE = 'tasks';

    public const PRIMARY_KEY = 'tid';

    public const STATUS_COMPLETE = 'Complete';
    public const STATUS_IN_PROGRESS = 'In progress';


    protected $tid;

    protected $name;

    protected $email;

    protected $text;

    protected $status = 0;

    protected $edited = 0;

    /**
     * @param mixed $name
     */
    public function setName($name): void
    {
        $this->name = $name;
    }

    /**
     * @param mixed $email
     */
    public function setEmail($email): void
    {
        $this->email = $email;
    }

    /**
     * @param mixed $text
     */
    public function setText($text): void
    {
        $this->text = trim($text);
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return mixed
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @return mixed
     */
    public function getText()
    {
        return $this->text;
    }

    /**
     * @return string
     */
    public function getStatus(): string
    {
        return $this->status;
    }

    /**
     * @param int $status
     */
    public function setStatus(int $status): void
    {
        $this->status = $status;
    }

    /**
     * @return mixed
     */
    public function getTid()
    {
        return $this->tid;
    }

    /**
     * @return int
     */
    public function getEdited(): int
    {
        return $this->edited;
    }

    /**
     * @param int $edited
     */
    public function setEdited(int $edited): void
    {
        $this->edited = $edited;
    }
}