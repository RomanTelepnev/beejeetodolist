<?php


namespace BeeJee\TodoList\Entity;


use BeeJee\TodoList\Core\App;
use BeeJee\TodoList\Core\IDB;
use BeeJee\TodoList\Core\IEntity;

abstract class Entity implements IEntity, \ArrayAccess
{
    public const TABLE = '';

    public const PRIMARY_KEY = '';

    protected static $query;

    public static function db(): IDB
    {
        return App::getDB();
    }

    public static function find($id): IEntity
    {
        $entity = null;
        $db = static::db();
        $entityProps = $db->select(static::TABLE)->where(static::PRIMARY_KEY, $id)->first();
        if ($entityProps) {
            return static::load($entityProps);
        }
        return $entity;
    }

    public static function select(): IDB
    {
        static::$query = static::db()->select(static::TABLE);
        return static::$query;
    }

    public function save()
    {
        $props = get_object_vars($this);
        $db = static::db();
        if (empty($this->{static::PRIMARY_KEY})) {
            $db->insert(static::TABLE, $props);
            $this->{static::PRIMARY_KEY} = $db->getLastId();
        } else {
            $db->update(static::TABLE, $props)
                ->where(static::PRIMARY_KEY, $this->{static::PRIMARY_KEY})
                ->execute();
        }
    }

    public static function total()
    {
        return static::select()->rowCount();
    }

    public static function fetchAll()
    {
        $entities = [];
        if (static::$query) {
            $result = static::$query->get();
            foreach ($result as $props) {
                $entities[] = static::load($props);
            }
        }
        return $entities;
    }

    protected static function load($entityProps): IEntity
    {
        $entity = new static();
        $props = array_keys(get_object_vars($entity));
        foreach ($props as $prop) {
            if (isset($entityProps[$prop])) {
                $entity->$prop = $entityProps[$prop];
            }
        }
        return $entity;
    }

    public static function query()
    {
        return static::$query;
    }

    /**
     * @inheritDoc
     */
    public function offsetExists($offset)
    {
        return isset($this->$offset);
    }

    /**
     * @inheritDoc
     */
    public function offsetGet($offset)
    {
        $value = null;
        $getter = 'get' . ucfirst($offset);
        if (method_exists($this, $getter)) {
            $value = $this->$getter();
        }
        return $value;
    }

    /**
     * @inheritDoc
     */
    public function offsetSet($offset, $value)
    {
        $setter = 'set' . ucfirst($offset);
        if (method_exists($this, $setter)) {
            $this->$setter($value);
        }
    }

    /**
     * @inheritDoc
     */
    public function offsetUnset($offset) {}
}