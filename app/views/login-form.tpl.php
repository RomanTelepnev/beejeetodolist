<h1 class="jumbotron-heading text-center">Log in</h1>
<?php if (!empty($validationErrors)): ?>
    <div class="alert alert-danger" role="alert">
        <ul>
            <?php foreach ($validationErrors as $error): ?>
                <li><?php print $error; ?></li>
            <?php endforeach; ?>
        </ul>
    </div>
<?php endif; ?>
<form action="/login" method="post">
    <div class="form-group">
        <label for="name">User name</label>
        <input type="text" class="form-control" id="name" name="name" value="<?php print $formValues['name']; ?>">
    </div>
    <div class="form-group">
        <label for="pass">Password</label>
        <input type="password" class="form-control" id="pass" name="pass" value="">
    </div>
    <input type="hidden" value="<?php print $formValues['token']; ?>" name="token">
    <div class="form-group">
        <button type="submit" class="btn btn-primary mb-2">Log in</button>
    </div>
</form>
