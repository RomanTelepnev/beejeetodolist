<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>BeeJee Todo list</title>

    <!-- Bootstrap core CSS -->
    <link href="/misc/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="/misc/css/album.css" rel="stylesheet">
    <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet">
</head>

<body>

<header>
    <div class="navbar navbar-dark bg-dark box-shadow">
        <div class="container d-flex justify-content-between">
            <a href="/" class="navbar-brand d-flex align-items-center">
                <strong>Home</strong>
            </a>
            <a href="<?php print $authLink; ?>" class="navbar-brand d-flex align-items-center">
                <strong><?php print $authLinkName; ?></strong>
            </a>
        </div>
    </div>
</header>

<main role="main">
    <?php if (!empty($content)): ?>
        <?php print $content; ?>
    <?php endif; ?>
    <section class="jumbotron">
        <div class="container">
            <?php if (!empty($messages)): ?>
                <div class="alert alert-success" role="alert">
                    <ul>
                        <?php foreach ($messages as $message): ?>
                            <li><?php print $message; ?></li>
                        <?php endforeach; ?>
                    </ul>
                </div>
            <?php endif; ?>
            <?php if(!empty($main)): ?>
                <?php print $main; ?>
            <?php endif; ?>
        </div>
    </section>
</main>

<footer class="text-muted">
    <div class="container">
        <p class="float-right">
            <a href="#">Back to top</a>
        </p>
        <p>Album example is &copy; Bootstrap, but please download and customize it for yourself!</p>
        <p>New to Bootstrap? <a href="../../">Visit the homepage</a> or read our <a href="../../getting-started/">getting started guide</a>.</p>
    </div>
</footer>
<!-- Bootstrap core JavaScript
    ================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="/misc/js/jquery-3.2.1.slim.min.js"></script>
<script src="/misc/js/popper.min.js"></script>
<script src="/misc/js/bootstrap.min.js"></script>
<script src="/misc/js/holder.min.js"></script>
</body>
</html>
