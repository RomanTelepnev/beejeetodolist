<div class="album py-5 bg-light">
  <div class="container">
    <div class="row">
      <table class="table table-bordered table-striped">
        <thead>
        <tr>
          <th scope="col" class="<?php print $filter['name']['class']; ?>"><a href="<?php print $filter['name']['link']; ?>">User<a> <?php print $filter['name']['icon']; ?></th>
          <th scope="col" class="<?php print $filter['email']['class']; ?>"><a href="<?php print $filter['email']['link']; ?>">Email<a> <?php print $filter['email']['icon']; ?></th>
          <th scope="col">Text</th>
          <th scope="col" class="<?php print $filter['status']['class']; ?>"><a href="<?php print $filter['status']['link']; ?>">Status<a> <?php print $filter['status']['icon']; ?></th>
          <?php if ($isAdmin): ?><th scope="col">Edit Link</th><?php endif; ?>
        </tr>
        </thead>
        <tbody>
        <?php foreach ($tasks as $task): ?>
          <tr>
            <td><?php print $task['name']; ?></td>
            <td><?php print $task['email']; ?></td>
            <td><?php print $task['text']; ?><?php if ($task['edited']): ?><br><span class="badge badge-warning">Edited by Admin</span><?php endif; ?></td>
            <td>
                <?php if (!empty($task['status'])): ?>
                  <span class="badge badge-success">Completed</span>
                <?php else: ?>
                  <span class="badge badge-info">In Progress</span>
                <?php endif; ?>
            </td>
            <?php if ($isAdmin): ?><td scope="col"><a href="/task/<?php print $task['tid']; ?>">Edit</a></td><?php endif; ?>
          </tr>
        <?php endforeach; ?>
        </tbody>
      </table>
    </div>
    <?php if (!empty($pager)): ?>
        <?php print $pager; ?>
    <?php endif; ?>
  </div>
</div>