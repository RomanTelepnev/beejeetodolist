<h1 class="jumbotron-heading text-center">Edit Task</h1>
<?php if (!empty($validationErrors)): ?>
  <div class="alert alert-danger" role="alert">
    <ul>
      <?php foreach ($validationErrors as $error): ?>
        <li><?php print $error; ?></li>
      <?php endforeach; ?>
    </ul>
  </div>
<?php endif; ?>
<?php if (!empty($messages)): ?>
  <div class="alert alert-success" role="alert">
    <ul>
        <?php foreach ($messages as $message): ?>
          <li><?php print $message; ?></li>
        <?php endforeach; ?>
    </ul>
  </div>
<?php endif; ?>
<form action="/<?php print $path; ?>" method="post">
    <div class="form-group">
        <label for="name">User name</label>
        <input disabled type="text" class="form-control" id="name" value="<?php print $formValues['name']; ?>">
    </div>
    <div class="form-group">
        <label for="email">Email address</label>
        <input disabled type="email" class="form-control" id="email" value="<?php print $formValues['email']; ?>">
    </div>
    <div class="form-group">
        <label for="text">Task Description</label>
        <textarea class="form-control" id="text" rows="3" name="text"><?php print $formValues['text']; ?></textarea>
    </div>
    <div class="form-check">
        <input class="form-check-input" type="checkbox" id="status" name="status" value="1" <?php if ($formValues['status']): ?> checked <?php endif; ?>>
        <label class="form-check-label" for="status">Completed</label>
    </div>
    <div class="form-group"></div>
    <input type="hidden" value="<?php print $formValues['token']; ?>" name="token">
    <div class="form-group">
        <button type="submit" class="btn btn-primary mb-2">Save</button>
    </div>
</form>
