<h1 class="jumbotron-heading text-center">Create Task</h1>
<?php if (!empty($validationErrors)): ?>
  <div class="alert alert-danger" role="alert">
    <ul>
      <?php foreach ($validationErrors as $error): ?>
        <li><?php print $error; ?></li>
      <?php endforeach; ?>
    </ul>
  </div>
<?php endif; ?>
<?php if (!empty($messages)): ?>
  <div class="alert alert-success" role="alert">
    <ul>
        <?php foreach ($messages as $message): ?>
          <li><?php print $message; ?></li>
        <?php endforeach; ?>
    </ul>
  </div>
<?php endif; ?>
<form action="/" method="post">
    <div class="form-group">
        <label for="name">User name</label>
        <input type="text" class="form-control" id="name" name="name" value="<?php print $formValues['name']; ?>">
    </div>
    <div class="form-group">
        <label for="email">Email address</label>
        <input type="email" class="form-control" id="email" name="email" placeholder="name@example.com" value="<?php print $formValues['email']; ?>">
    </div>
    <div class="form-group">
        <label for="text">Task Description</label>
        <textarea class="form-control" id="text" rows="3" name="text"><?php print $formValues['text']; ?></textarea>
    </div>
    <input type="hidden" value="<?php print $formValues['token']; ?>" name="token">
    <div class="form-group">
        <button type="submit" class="btn btn-primary mb-2">Create</button>
    </div>
</form>
