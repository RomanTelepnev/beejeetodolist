<nav aria-label="Task pager">
    <ul class="pagination justify-content-center">
        <li class="page-item <?php if ($isFirst):?> disabled <?php endif; ?>">
            <a class="page-link" href="<?php print $prevLink; ?>" tabindex="-1">Previous</a>
        </li>
        <?php for ($i = 0; $i < $pages; $i++): ?>
            <li class="page-item <?php if ($i === $page): ?> active <?php endif; ?>"><a class="page-link" href="<?php print $pageLinks[$i]; ?>"><?php print $i + 1; ?></a></li>
        <?php endfor; ?>
        <li class="page-item <?php if ($isLast):?> disabled <?php endif; ?>">
            <a class="page-link" href="<?php print $nextLink; ?>">Next</a>
        </li>
    </ul>
</nav>