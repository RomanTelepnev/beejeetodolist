<?php

use BeeJee\TodoList\Core\App;

require_once __DIR__ . '/vendor/autoload.php';

$app = new App();
$response = $app->processRequest();
$response->send();